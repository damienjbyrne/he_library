# Health Engine Personal Library

Implementation of a very basic personal library application in C++ for Health Engine.

## Getting Started

Check out the code from Bitbucket if you haven't already.

Follow the build instructions below to create an executable and a test object. 

Original requirements and some other notes are in the [REQUIREMENTS.md](./REQUIREMENTS.md) file.

### Prerequisites

* c++ build environment supporting C++11 - The choice of C++11 came from my previous experience being with C++98 from 
   seven years ago and deciding to use a more recent flavour but not so exactly recent.  

* cmake 3.7+ - The build configuration is in cmake.

*Note: This code has been developed on a Linux system (Ubuntu 17.04) and has not been tested on Windows or OSX.*

### Installing

Build the build config cache using cmake.

```
> cmake CMakeLists.txt
```

Then make the project:

```
> cmake --build . --target all
```

To run the actual program, change to the `bin` directory and run the executable.  The executable expects to be run from 
`bin` to access the data file correctly.

```
> cd bin
bin> ./HEPersonalLibrary
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Once the test set is built using the instructions above, run the test suite with:

```
> cd bin
bin> ./HEPL_test
```

The tests also expect to run under the `bin` directory for accessing some test files.

### Test extent and limitations

The tests in this project have been written using the **Catch** library - https://github.com/philsquared/Catch

Tests have been written in a BDD style and form a part of the documentation of this application.

The catch library has some addition output, to see what is available use:

```
> cd bin
bin> ./HEPL_test -h
```

The test for the persistence object is simplified and needs to handle resetting the write test and confirming the 
output is written.

The controller object is not automated tested.  I had not had time to work out a test strategy for it.
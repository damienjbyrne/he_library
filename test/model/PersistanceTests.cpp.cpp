//
// Created by damien on 17/09/17.
//

#include "../catch.hpp"
#include "../../src/model/Library.h"
#include "../../src/model/Persistence.h"


SCENARIO("Loading a library reads from a file and loads books into a library", "[persistance][integration]") {

    GIVEN("A persistance object initialised with an empty library") {
        Library library = Library();
        std::string pathToStorageFile = "../test/data/library-load.data";
        Persistence persistence = Persistence(library, pathToStorageFile);

        WHEN("a file location is passed to the load function") {
            persistence.loadFromFile();
            THEN("the library is populated with books") {
                Book book = Book("Fifth book", "First author");
                auto list = library.getBookList();
                REQUIRE(list.size() == 5);
                REQUIRE(list[4] == book);
            }
        }
    }
}

SCENARIO("Saving the library writes the library contents out to a file", "[persistance][integration]") {

    GIVEN("A persistance object initialised with a populated library") {
        Library library = Library();
        library.addBook(Book("First book", "First author"));
        library.addBook(Book("Second book", "Second author"));
        library.addBook(Book("Third book", "Third author"));
        library.addBook(Book("Fourth book", "Fourth author"));
        library.addBook(Book("Fifth book", "Fifth author"));
        library.addBook(Book("Sixth book", "Sixth author"));

        std::string pathToStorageFile = "../test/data/library-save.data";
        Persistence persistence = Persistence(library, pathToStorageFile);

        // TODO: make sure the output file is empty or not there

        WHEN("a file location is passed to the load function") {
            persistence.saveToFile();
            THEN("the library is populated with books") {
                // TODO: check the file for output
            }
        }
    }
}
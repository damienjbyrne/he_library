//
// Created by damien on 15/09/17.
//
#include "../catch.hpp"
#include "../../src/model/Book.h"
#include "../../src/model/Library.h"

#include <string>

SCENARIO("Libraries return an empty list of books when no books have been added", "[Library][unit]") {

    GIVEN("An empty library") {
        Library library = Library();

        WHEN("the list is requested") {
            std::vector<Book> bookList = library.getBookList();
            THEN("an empty array is returned") {
                REQUIRE(bookList.empty());
            }
        }
    }
}

SCENARIO("Libraries can add books", "[Library][unit]") {

    GIVEN("A library") {
        Library library = Library();

        WHEN("a book is added and the list requested") {
            Book book = Book("A Title", "An author");
            library.addBook(book);
            std::vector<Book> bookList = library.getBookList();
            THEN("the book is returned in the list") {
                REQUIRE(bookList.size() == 1);
                REQUIRE(bookList.front() == book);
            }
        }
    }
}

SCENARIO("Libraries can remove books", "[Library][unit]") {

    GIVEN("A library with a book already added") {
        Library library = Library();
        Book book = Book("A Title", "An author");
        library.addBook(book);

        WHEN("the book title and description are requested to be removed") {
            library.removeBookByTitleAndAuthor("A Title", "An author");
            THEN("the library is empty") {
                std::vector<Book> bookList = library.getBookList();
                REQUIRE(bookList.empty());
            }
        }

        WHEN("a book object is requested to be removed") {
            library.removeBook(book);
            THEN("the library is empty") {
                std::vector<Book> bookList = library.getBookList();
                REQUIRE(bookList.empty());
            }
        }
    }
}

SCENARIO("Libraries can find a book by title", "[Library][unit]") {

    GIVEN("A library with books") {
        Library library = Library();
        library.addBook(Book("First book", "First author"));
        library.addBook(Book("Second book", "Second author"));
        library.addBook(Book("Third book", "Third author"));
        library.addBook(Book("Fourth book", "Fourth author"));
        library.addBook(Book("Fifth book", "Fifth author"));
        library.addBook(Book("Sixth book", "Sixth author"));

        WHEN("a search for title 'Third book' is made") {
            std::vector<Book> result = library.findBooksByTitle("Third book");
            Book book = Book("Third book", "Third author");
            THEN("the book is returned in the results") {
                REQUIRE(result.size() == 1);
                REQUIRE(result.front() == book);
            }
        }

        WHEN("a search for title 'Fifth book' is made") {
            std::vector<Book> result = library.findBooksByTitle("Fifth book");
            Book book = Book("Fifth book", "Fifth author");
            THEN("the book is returned in the results") {
                REQUIRE(result.size() == 1);
                REQUIRE(result.front() == book);
            }
        }

        WHEN("a search for a partial title 'Fi' is made") {
            std::vector<Book> result = library.findBooksByTitle("Fi");
            Book book1 = Book("First book", "First author");
            Book book2 = Book("Fifth book", "Fifth author");
            THEN("the books are returned in the results") {
                REQUIRE(result.size() == 2);
                REQUIRE(result[0] == book1);
                REQUIRE(result[1] == book2);
            }
        }
    }
}

SCENARIO("Libraries can find a book by author", "[Library][unit]") {

    GIVEN("A library with books") {
        Library library = Library();
        library.addBook(Book("First book", "First author"));
        library.addBook(Book("Second book", "Second author"));
        library.addBook(Book("Third book", "Second author"));
        library.addBook(Book("Fourth book", "Third author"));
        library.addBook(Book("Fifth book", "Third author"));
        library.addBook(Book("Sixth book", "Third author"));
        library.addBook(Book("Seventh book", "Fourth author"));
        library.addBook(Book("Eighth book", "Fifth author"));
        library.addBook(Book("Ninth book", "Fifth author"));
        library.addBook(Book("Tenth book", "Fifth author"));

        WHEN("a search for author 'Second author' is made") {
            std::vector<Book> result = library.findBooksByAuthor("Second author");
            Book expectedBook1 = Book("Second book", "Second author");
            Book expectedBook2 = Book("Third book", "Second author");
            THEN("the booka are returned in the results") {
                REQUIRE(result.size() == 2);
                REQUIRE(result[0] == expectedBook1);
                REQUIRE(result[1] == expectedBook2);
            }
        }

        WHEN("a search for author 'Fourth author' is made") {
            std::vector<Book> result = library.findBooksByAuthor("Fourth author");
            Book expectedBook = Book("Seventh book", "Fourth author");
            THEN("the book is returned in the results") {
                REQUIRE(result.size() == 1);
                REQUIRE(result.front() == expectedBook);
            }
        }

        WHEN("a search for a partial author 'Fi' is made") {
            std::vector<Book> result = library.findBooksByAuthor("Fi");
            Book expectedBook1 = Book("First book", "First author");
            Book expectedBook2 = Book("Eighth book", "Fifth author");
            Book expectedBook3 = Book("Ninth book", "Fifth author");
            Book expectedBook4 = Book("Tenth book", "Fifth author");
            THEN("the books are returned in the results") {
                REQUIRE(result.size() == 4);
                REQUIRE(result[0] == expectedBook1);
                REQUIRE(result[1] == expectedBook2);
                REQUIRE(result[2] == expectedBook3);
                REQUIRE(result[3] == expectedBook4);
            }
        }
    }
}

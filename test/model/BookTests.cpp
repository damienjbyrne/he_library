//
// Created by damien on 14/09/17.
//
#include "../catch.hpp"
#include "../../src/model/Book.h"

SCENARIO("Books have a title and an author", "[Book][unit]") {

    GIVEN("A Book") {
        Book book = Book("A book", "An author");

        WHEN("the title is requested") {
            THEN("the title is returned") {
                REQUIRE(book.getTitle() == "A book");
            }
        }
        WHEN("the author is requested") {
            THEN("the author is returned") {
                REQUIRE(book.getAuthor() == "An author");
            }
        }
    }
}

SCENARIO("Books can confirm their equality", "[Book][unit]") {

    GIVEN("A Book") {
        Book book = Book("A book", "An author");

        WHEN("compared to the same book") {
            Book book2 = book;
            THEN("it confirms they are equal") {
                REQUIRE(book == book2);
            }
        }

        WHEN("compared a book with the same title and author") {
            Book book2 = Book("A book", "An author");
            THEN("it confirms they are equal") {
                REQUIRE(book == book2);
            }
        }

        WHEN("compared a book with different title and same author") {
            Book book2 = Book("A different book", "An author");
            THEN("it confirms they are not equal") {
                REQUIRE(!(book == book2));
            }
        }

        WHEN("compared a book with the same title and different author") {
            Book book2 = Book("A book", "Another author");
            THEN("it confirms they are not equal") {
                REQUIRE(!(book == book2));
            }
        }
    }
}
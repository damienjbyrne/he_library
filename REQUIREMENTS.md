# Requrements and Specifications

## Initial problem description

### Objective

Using your preferred programming language, provide a solution to manage your
personal library where you can add , remove and lookup Books by Title and/or
Author(s).

### App Requirements

* You are free to use a programming language. Use of opinionated PHP,
JavaScript, Java, C++ or C# is preferred. (C++ specifically requested).
* You are not required to build a Graphical User Interface.

### Additional Requirements

* Please ensure your submission is documented appropriately, stating
assumptions and algorithmic complexity of each operation. Be specific.
* You may use pre-existing frameworks and collections.

## Assumptions

* Book list is stored as a file.
* Book list is loaded on program load and saved on close.
* Book list is loaded completely into memory during runtime.
* Books only record Title and Author and these are the only elements to use for
  matching.
  
## Limitations

As stated elsewhere I have been recovering my C++98 knowledge from seven years ago and trying to update it with C++11isms.
I am not fully confident that the usages of reference passing and object copying are the most efficient methods and would
probably want to revisit them with further testing.

The persistence object only prints out errors to the console if it cannot find files.  This should be changed to throwing
exceptions and having a catch to process the errors correctly.

*Note, have added exceptions to the persistence object and put the execution in a try/catch block.*
  
## Performance notes
* The library uses a std::vector at it's and returns a new vector when returning the list.  Most of the actions on the 
  vector are either O(1) for things like .size() and .empty() and pushing new elements onto the vector.  Other actions like
  searching and iterating are O(n).
  
* Adding elements using a standard vector push with no re-sorting so this is O(1).  If adding in correct order was used,
  this would increase to O(n).
  
* Removing an element involves a sweep of the vector to find matching elements so a removal is O(n).

* Getting the book list creates a new vector to return, this is a O(n) operation.

* Searching the list creates a new vector and does a sweep of the vector to build the new one, this is also O(n).

* Improving writes for adding new books in order could be sped up if using a std::map or a std::list.

* Improvements for searching by author could be improved by using a std::map of authors with each holding a vector of books.

 * After further reading I'm thinking that a multimap might be better for the library `std::multimap<std::string, Book>`
   using the Author as the multimap key.  Then searches can either be direct iterations over the map or for search by author, 
   get the sub-list using librarym.equal_range(author) then iterate over that sublist.  This would need to be performance tested.
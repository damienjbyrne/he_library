cmake_minimum_required(VERSION 3.7)
project(HealthEnginePersonalLibrary)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin")

set(SOURCE_FILES src/main.cpp src/model/Book.cpp src/model/Book.h src/model/Library.cpp src/model/Library.h src/model/Persistence.cpp src/model/Persistence.h src/controller/LibraryController.cpp src/controller/LibraryController.h)
add_executable(HEPersonalLibrary ${SOURCE_FILES})

set(TEST_SOURCE_FILES test/catch.hpp test/test_main.cpp test/model/BookTests.cpp src/model/Book.cpp src/model/Book.h test/model/LibraryTests.cpp src/model/Library.cpp src/model/Library.h test/model/PersistanceTests.cpp.cpp src/model/Persistence.cpp src/model/Persistence.h src/controller/LibraryController.cpp src/controller/LibraryController.h)
add_executable(HEPL_test ${TEST_SOURCE_FILES})

//
// Created by damien on 17/09/17.
//

#include "LibraryController.h"

LibraryController::LibraryController(Library &library) : library(library) {}

void LibraryController::executeCommandLoop() {
    bool keepRunningLibrary = true;

    while (keepRunningLibrary) {
        std::cout << "--------------------------------------------------------------------" << std::endl;
        std::cout << "A - Add a book; R - Remove a book; L - List the books" << std::endl;
        std::cout << "T - Search by Title; W - Search by Writer (Author); X - Exit" << std::endl;
        std::cout << "Enter a command: ";
        std::string commandInput;
        getline(std::cin, commandInput);
        switch (commandInput[0]) {
            case 'A':
            case 'a':
                addBookCommand();
                break;
            case 'R':
            case 'r':
                removeBookCommand();
                break;
            case 'L':
            case 'l':
                listBooksCommand();
                break;
            case 'T':
            case 't':
                searchForBookByTitleCommand();
                break;
            case 'W':
            case 'w':
                searchForBookByAuthorCommand();
                break;
            case 'X':
            case 'x':
                keepRunningLibrary = false;
                break;
            default:
                std::cout << "Unknown command, try again." << std::endl;
                break;
        }
    }
}

void LibraryController::addBookCommand() {
    std::string newTitle;
    std::string newAuthor;
    std::cout << "Creating a new book entry" << std::endl;
    std::cout << "Enter a title:";
    getline(std::cin, newTitle);
    std::cout << "Enter an author:";
    getline(std::cin, newAuthor);

    library.addBook(Book(newTitle, newAuthor));
}

void LibraryController::removeBookCommand() {
    std::string findTitle;
    std::string findAuthor;
    std::cout << "Enter details of book to remove (must match exactly)" << std::endl;
    std::cout << "Enter a title:";
    getline(std::cin, findTitle);
    std::cout << "Enter an author:";
    getline(std::cin, findAuthor);

    library.removeBookByTitleAndAuthor(findTitle, findAuthor);
}

void LibraryController::listBooksCommand() {
    auto list = library.getBookList();
    displayList(list);
}

void LibraryController::searchForBookByTitleCommand() {
    std::string findTitle;
    std::cout << "Enter a title search:";
    getline(std::cin, findTitle);
    auto list = library.findBooksByTitle(findTitle);
    displayList(list);
}

void LibraryController::searchForBookByAuthorCommand() {
    std::string findAuthor;
    std::cout << "Enter an author search:";
    getline(std::cin, findAuthor);
    auto list = library.findBooksByAuthor(findAuthor);
    displayList(list);
}

void LibraryController::displayList(const std::vector<Book> &list) const {
    std::cout << "--------------------------------------------------------------------" << std::endl;
    std::cout << "Current book list:" << std::endl;

    for (Book const &book: list) {
        std::cout << book.getTitle() << "; " << book.getAuthor() << std::endl;
    }
}

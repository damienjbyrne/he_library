//
// Created by damien on 17/09/17.
//

#ifndef HEALTHENGINEPERSONALLIBRARY_LIBRARYCONTROLLER_H
#define HEALTHENGINEPERSONALLIBRARY_LIBRARYCONTROLLER_H

#include "../model/Library.h"
#include "../model/Persistence.h"

class LibraryController {
    Library &library;

public:
    explicit LibraryController(Library &library);

    void executeCommandLoop();

private:
    void addBookCommand();

    void removeBookCommand();

    void listBooksCommand();

    void searchForBookByTitleCommand();

    void searchForBookByAuthorCommand();

    void displayList(const std::vector<Book> &list) const;
};


#endif //HEALTHENGINEPERSONALLIBRARY_LIBRARYCONTROLLER_H

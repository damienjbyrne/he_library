//
// Created by damien on 17/09/17.
//

#include "Persistence.h"

Persistence::Persistence(Library &library, std::string &pathToStorageFile) :
        library(library),
        pathToStorageFile(pathToStorageFile) {}

void Persistence::loadFromFile() {
    std::string inputLine;
    std::ifstream libraryFile(pathToStorageFile);

    if (!libraryFile.good()) {
        throw std::runtime_error("Failed to load file " + pathToStorageFile);
    }

    while (getline(libraryFile, inputLine)) {
        std::vector<std::string> lineElements;
        tokenize(inputLine, lineElements, ";");
        library.addBook(Book(lineElements[0], lineElements[1]));
    }

    libraryFile.close();
}

void Persistence::saveToFile() {
    std::ofstream libraryFile(pathToStorageFile);

    if (!libraryFile.good()) {
        throw std::runtime_error("Error opening file to write to " + pathToStorageFile);
    }

    auto list = library.getBookList();
    for (Book const &book: list) {
        libraryFile << book.getTitle() << ";" << book.getAuthor() << std::endl;

    }

    libraryFile.close();

}

void Persistence::tokenize(const std::string &str, std::vector<std::string> &tokens, const std::string &delimiters) {
    std::string::size_type startPos = str.find_first_not_of(delimiters, 0);
    std::string::size_type endPos = str.find_first_of(delimiters, startPos);

    while (std::string::npos != endPos || std::string::npos != startPos) {
        tokens.push_back(str.substr(startPos, endPos - startPos));
        startPos = str.find_first_not_of(delimiters, endPos);
        endPos = str.find_first_of(delimiters, startPos);
    }
}

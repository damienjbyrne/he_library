//
// Created by damien on 15/09/17.
//

#ifndef HEALTHENGINEPERSONALLIBRARY_BOOK_H
#define HEALTHENGINEPERSONALLIBRARY_BOOK_H

#include <string>

class Book {
private:
    std::string title = "";
    std::string author = "";

public:
    Book(const std::string &title, const std::string &author);

    friend bool operator==(Book lhs, Book rhs);

    const std::string &getTitle() const;

    const std::string &getAuthor() const;
};


#endif //HEALTHENGINEPERSONALLIBRARY_BOOK_H

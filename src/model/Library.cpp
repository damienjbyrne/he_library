//
// Created by damien on 15/09/17.
//
#include "Library.h"


Library::Library() {
    books = std::vector<Book>();
}

std::vector<Book> Library::getBookList() {
    return books;
}

void Library::addBook(const Book &book) {
    books.push_back(book);
}

void Library::removeBookByTitleAndAuthor(const std::string &title, const std::string &author) {
    Book book = Book(title, author);
    removeBook(book);
}

void Library::removeBook(const Book &book) {
    books.erase(std::remove_if(books.begin(),
                               books.end(),
                               [&book](const Book &test) { return test == book; }),
                books.end());
}

std::vector<Book> Library::findBooksByTitle(const std::string &title) {
    auto it = books.begin();
    std::vector<Book> returnResult = std::vector<Book>();

    while (it != books.end()) {
        it = std::find_if(it,
                          books.end(),
                          [&title](const Book &book) { return book.getTitle().find(title) != std::string::npos; });
        if (it != books.end()) {
            returnResult.push_back(*it);
            it++;
        }
    }
    return returnResult;
}

std::vector<Book> Library::findBooksByAuthor(const std::string &author) {
    auto it = books.begin();
    std::vector<Book> returnResult = std::vector<Book>();

    while (it != books.end()) {
        it = std::find_if(it,
                          books.end(),
                          [&author](const Book &book) { return book.getAuthor().find(author) != std::string::npos; });
        if (it != books.end()) {
            returnResult.push_back(*it);
            it++;
        }
    }
    return returnResult;
}

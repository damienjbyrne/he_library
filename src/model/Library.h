//
// Created by damien on 15/09/17.
//

#ifndef HEALTHENGINEPERSONALLIBRARY_LIBRARY_H
#define HEALTHENGINEPERSONALLIBRARY_LIBRARY_H

#include <algorithm>
#include <vector>
#include "Book.h"

class Library {
    std::vector<Book> books;

public:
    Library();

    void addBook(const Book &book);

    void removeBook(const Book &book);
    void removeBookByTitleAndAuthor(const std::string &title, const std::string &author);

    /**
     *
     * @return
     */
    std::vector<Book> getBookList();

    /**
     *
     * @param title A title or partial title to search the library for.
     * @return A vector containing the book objects found.
     */
    std::vector<Book> findBooksByTitle(const std::string &title);

    /**
     *
     * @param author A name or name partial of an author to search by.
     * @return A vector containing the book objects found.
     */
    std::vector<Book> findBooksByAuthor(const std::string &author);
};

#endif //HEALTHENGINEPERSONALLIBRARY_LIBRARY_H

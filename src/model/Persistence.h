//
// Created by damien on 17/09/17.
//

#ifndef HEALTHENGINEPERSONALLIBRARY_PERSISTANCE_H
#define HEALTHENGINEPERSONALLIBRARY_PERSISTANCE_H


#include "Library.h"
#include <fstream>
#include <utility>
#include <string>
#include <stdexcept>
#include <iostream>

class Persistence {
    Library &library;
    std::string pathToStorageFile;

public:
    Persistence(Library &library, std::string &pathToStorageFile);

    void loadFromFile();

    void saveToFile();

private:
    void tokenize(const std::string &str, std::vector<std::string> &tokens, const std::string &delimiters);

};


#endif //HEALTHENGINEPERSONALLIBRARY_PERSISTANCE_H

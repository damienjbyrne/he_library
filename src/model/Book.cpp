//
// Created by damien on 15/09/17.
//

#include "Book.h"

Book::Book(const std::string &title, const std::string &author) : title(title), author(author) {}

bool operator==(Book lhs, Book rhs) {
    bool isEqual = (lhs.getTitle() == rhs.getTitle()) &&
                   (lhs.getAuthor() == rhs.getAuthor());

    return isEqual;
}

const std::string &Book::getTitle() const {
    return title;
}

const std::string &Book::getAuthor() const {
    return author;
}

#include "../src/model/Book.h"
#include "../src/model/Library.h"
#include "model/Persistence.h"
#include "controller/LibraryController.h"
#include <stdexcept>

int main() {
    Library library = Library();
    std::string pathToStorageFile = "../data/library.data";
    Persistence persistence = Persistence(library, pathToStorageFile);
    LibraryController libraryController = LibraryController(library);

    try {
        persistence.loadFromFile();
        libraryController.executeCommandLoop();
        persistence.saveToFile();
    } catch (const std::exception &e) {
        std::cout << e.what() << std::endl;
        return 1;
    }

    return 0; // success return
}